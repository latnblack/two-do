// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var twodo = angular.module('twodo', ['ionic'])
/*
.config(['$stateProvider','$urlRouterProvider', function($stateProvider,$urlRouterProvider) {
  $stateProvider
  .state('intro', {
    url: '/intro',
    templateUrl: 'intro.html',
    controller: 'IntroCtrl'
  })
  .state('main', {
    url: '/main',
    templateUrl: 'index.html',
    controller: 'TwoDoListCtrl'
  })
}])
*/
.directive('hasDetails', function($ionicGesture, $ionicModal) {
  return {
    controller: function($scope) {
      $ionicModal.fromTemplateUrl('views/task-details.html', function(modal) {
        $scope.taskDetailsModal = modal;
      }, {
        scope: $scope,
        animation: 'slide-in-right'
      });
    },
    link: function(scope, element, attrs) {
      $ionicGesture.on('swipeleft', function() {
        scope.taskDetailsModal.show();
      }, element);
      scope.closeDetails = function() {
        scope.taskDetailsModal.hide();
      };

      var listening = scope.$on('closeDetailsModal', function() {
        scope.taskDetailsModal.hide();
      });

      scope.$on('$destroy', function() {
        listening();
      });
    }
  }
})

.directive('modalSlideClosed', function($ionicGesture) {
  return {
    link: function(scope, element, attrs) {
      $ionicGesture.on('swiperight', function() {
        scope[attrs.modalSlideClosed].hide();
      }, element);
    }
  }
})

.directive('modalSlideDown', function($ionicGesture) {
  return {
    link: function(scope, element, attrs) {
      $ionicGesture.on('swipedown', function() {
        scope[attrs.modalSlideDown].hide();
      }, element);
    }
  }
})

.directive('tutorial', function($ionicGesture, $ionicPosition) {
  return {
    restrict: 'A',
    templateUrl: function(element, attrs) {
      return 'views/'+attrs.tutorial+'.html';
    },
    link: function(scope, element, attrs) {

    }
  }
})

.directive('watchMenu', function($ionicSideMenuDelegate) {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      scope.$watch(function() {
        if (scope.$parent.activeTutorial) return;
        var ratio = $ionicSideMenuDelegate.getOpenRatio();
        if (ratio == 1) {
          if (scope.$eval(attrs.primaryCondition)) {
            scope[attrs.watchMenu](attrs.primaryAction);
          }
          else if (attrs.secondaryCondition) {
            scope[attrs.watchMenu](attrs.secondaryAction);
          }
        }
      })
    }
  }
})

.directive('twodoPositionRelativeTo', function($ionicPosition) {
  return {
    restrict: 'A',
    scope: {
      'anchor': '@twodoPositionRelativeTo'
    },
    link: function(scope, element, attrs) {
      var anchor = angular.element(document.getElementById(scope.anchor));

      if (anchor.length) {
        var pos = $ionicPosition.position(anchor);
        var left = pos.left+(pos.width/2);
        var top = pos.top +pos.height;

        if (parseInt(attrs.topPadding)) {
          top = top + parseInt(attrs.topPadding);
        }

        if (parseInt(attrs.leftPadding)) {
          left = left + parseInt(attrs.leftPadding);
        }

        document.getElementsByTagName('head')[0].appendChild(document.createElement('style'));
        var myStyleSheet = document.styleSheets[document.styleSheets.length-1];
        var myRule = attrs.target+' { position: absolute; left: '+left+'px; top: '+top+'px }';
        if (myStyleSheet.insertRule) {
            myStyleSheet.insertRule(myRule, myStyleSheet.cssRules.length);
        }
      }
    }
  }
})

.run(function($ionicPlatform, Settings) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)

    if (!Settings.tutorials.all()) {
      Settings.tutorials.create();
    }
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});