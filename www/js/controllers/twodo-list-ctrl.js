var twodo = angular.module('twodo')
.controller('TwoDoListCtrl', function($scope, $ionicModal, $timeout, Projects, $ionicSideMenuDelegate, $timeout, $filter, $ionicActionSheet, $ionicBackdrop, Settings) {

  // Set defaults...
  $scope.lastTouch = false;
  $scope.hideCompleted = false;
  $scope.hideIncompleted = false;
  $scope.extendIncomplete = false;
  $scope.projects = Projects.all() || [];
  $scope.tutorials = Settings.tutorials.all();
  $scope.activeTutorial = false;

  $scope.toggleHideCompleted = function() {
    $scope.hideCompleted = !$scope.hideCompleted;
  };

  /** Tutorial stuff **/
  // Tutorial Overlay
  $scope.showTutorial = function(name) {
    if (!$scope.tutorials[name]) {
      if (name == 'project-list' && !$ionicSideMenuDelegate.isOpen()) {
        $ionicSideMenuDelegate.toggleLeft();
      }
      $timeout(function() {
        $ionicBackdrop.retain();
        $scope.activeTutorial = name;
        angular.element(document.getElementById('tut-'+name)).toggleClass('ng-hide');
      });
    }
  };
  $scope.closeTutorial = function(name) {
    angular.element(document.getElementById('tut-'+name)).toggleClass('ng-hide');
    $ionicBackdrop.release();
    $scope.activeTutorial = false;
    Settings.tutorials.update(name, true);
    $scope.tutorials[name] = true;
    if ($scope.projects.length == 0) {
      $scope.firstList = true;
    }
  };
  /** Tutorial stuff end **/

  /** Modal stuff **/
  // Create task modal
  $ionicModal.fromTemplateUrl('views/new-task.html', function(modal) {
    $scope.taskModal = modal;
  }, {
    scope: $scope,
    animation: 'slide-in-up'
  });

  // Update task modal
  $ionicModal.fromTemplateUrl('views/new-task.html', function(modal) {
    $scope.editTaskModal = modal;
  }, {
    scope: $scope,
    animation: 'slide-in-up'
  });

  // Create project modal
  $ionicModal.fromTemplateUrl('views/new-project.html', function(modal) {
    $scope.projectModal = modal;
  }, {
    scope: $scope,
    animation: 'slide-in-up'
  });
  /** Modal stuff end **/

  /** Project stuff **/
  // Projects
  $scope.createProject = function(project) {
    if ($scope.projects == undefined || $scope.projects.length < 1) {
      $scope.projects = [];
    }
    var newProject = Projects.newProject(project);
    $scope.projects.push(newProject);
    Projects.saveProject(newProject);
    $scope.projectModal.hide();
    project.title = '';
    project.description = '';

    $timeout(function() {
      if ($scope.tutorials['project-list']) {
        $scope.newTask();
      }
      $scope.showTutorial('project-list');
    }, 500);
    $scope.selectProject(newProject, $scope.projects.length-1);
  }

  // Load or initialize projects
  $scope.projects = Projects.all();

  // Grab the last active, or the first project
  $scope.activeProject = $scope.projects[Projects.getLastActiveIndex()] || $scope.projects[0];

  // Called to create a new project
  $scope.newProject = function() {
    $scope.projectModal.show();
  };

  // Close modal
  $scope.cancelNewProject = function() {
    $scope.projectModal.hide();
  };

  // Called to select the given project
  $scope.selectProject = function(project) {
    if (!project || project == undefined) {
      return;
    }
    project = Projects.setVisibleTasks(Projects.all()[project.index]);
    Projects.setLastActiveIndex(project.index);
    Projects.setLastActive(project);
    $scope.activeProject = project;
    if (!$scope.firstList) {
      $ionicSideMenuDelegate.toggleLeft(false);
    }
  };

  $scope.showProjectOptions = function(project) {
    var hideOptions = $ionicActionSheet.show({
      destructiveText: 'Delete \''+project.title+'\'',
      cancelText: '<span class="twodo-cancel">Cancel</span>',
      cancel: function() {
        hideOptions();
      },
      destructiveButtonClicked: function(index) {
        Projects.remove($scope, project);
        $timeout(function(){
          $scope.projects = Projects.all();
          if ($scope.projects.length == 0) {
            $scope.newProject();
          }
          else {
            $scope.selectProject($scope.projects[0], $scope.projects[0].index);
          }
          $scope.$apply();
          hideOptions();
        });
      }
    });
  }
  /** Project stuff end **/

  /** Task stuff **/
  // Called on submit
  $scope.saveTask = function(task, multi) {
    var projects = Projects.all(),
      projectIndex = $scope.activeProject.index,
      modal = 'taskModal';
      status;

    if (typeof task.index == 'number') {
      var status = task.completed ? 'completed' : 'incomplete';
      projects[projectIndex].tasks[status][task.index] = task;
      modal = 'editTaskModal';
    }
    else {
      projects[projectIndex].tasks['incomplete'].push({
        title: task.title,
        description: task.description,
        completed: false,
        index: $scope.activeProject.tasks['incomplete'].length
      });
    }

    Projects.save(projects);
    $scope.activeProject = projects[projectIndex];
    $scope.activeProject = Projects.setVisibleTasks($scope.activeProject);
    Projects.setLastActive($scope.activeProject);

    if (!status) {
      task.title = '';
      task.description = '';
    }

    $timeout(function() {
      $scope.$apply(function() {
        if (multi !== true) {
          $scope[modal].hide();
          $scope.showTutorial('task-list');
        }
      });
    })
  };

  $scope.deleteTask = function(task) {
    $scope.activeProject = Projects.removeTask($scope.activeProject, task);
    $scope.$broadcast('closeDetailsModal');
  };

  $scope.newTask = function() {
    if ($scope.task && typeof $scope.task.index == 'number') {
      $scope.task = {}
    }
    $scope.taskModal.show();
    $scope.showTutorial('task-create');
  };

  $scope.editTask = function(task) {
    $scope.task = task;
    $scope.editTaskModal.show();
  };

  $scope.cancelNewTask = function(task) {
    var modal = 'taskModal';
    if (task && typeof task.index == 'number') {
      modal = 'editTaskModal';
    }
    $scope[modal].hide();
  };

  $scope.toggleTaskCompleted = function(activeProject, status, taskIndex) {
    var projects = Projects.all(),
      projectIndex = activeProject.index,
      project = projects[projectIndex],
      isCompleted = status == 'completed' ? false : Date.now(),
      from = isCompleted ? 'incomplete' : 'completed',
      to = isCompleted ? 'completed' : 'incomplete'; 

    project = Projects.moveTask(project, activeProject.tasks[status][taskIndex], from, to);
    projects[projectIndex] = project;
    Projects.save(projects);
    $scope.activeProject = Projects.setVisibleTasks(project);
    Projects.setLastActive($scope.activeProject);

    $timeout(function() {
      $scope.$apply();
    });
  };

  $scope.repositionTask = function(project, task, fromIndex, toIndex) {
    project.tasks.incomplete.splice(fromIndex, 1);
    project.tasks.incomplete.splice(toIndex, 0, task);

    Projects.saveProject(project);
    $scope.activeProject = project;
    $timeout(function() {
      $scope.$apply();
    })
  }

  var organiseActiveList = function() {
    var incompleteCount = 0;
    var twodo = angular.element(document.getElementById('twodo-list'));
    angular.forEach(angular.element(twodo.children()[0]).children(), function(item) {
      var o = angular.element(item);
    });
  };

  $scope.showAllIncomplete = function(project) {
    if (!$scope.extendIncomplete) {
      $scope.activeProject = Projects.showAllIncomplete(project);
      $scope.extendIncomplete = true;
    }
    else {
      $scope.activeProject = Projects.setVisibleTasks(project);
      $scope.extendIncomplete = false;
    }
    $timeout(function() {
      $scope.$apply();
    })
  };
  /** Task stuff end **/

  /** Set up some stuff **/
  $scope.activeProject = Projects.all()[0];
  if ($scope.activeProject) {
    $scope.activeProject = Projects.setVisibleTasks($scope.activeProject);
  }

  $timeout(function() {
    $scope.$apply();
  });

  function onMenuKeyDown() {
    $ionicSideMenuDelegate.toggleLeft();
  };
  document.addEventListener('menubutton', onMenuKeyDown, false);
});