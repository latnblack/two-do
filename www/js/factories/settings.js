var twodo = angular.module('twodo')
.factory('Settings', function($window) {
  return {
    tutorials: {
      create: function() {
        var tuts = {
          intro: false,
          'project-create': false,
          'project-list': false,
          'task-create': false,
          'task-list': false
        };
        this.save(tuts);
        return tuts;
      },
      all: function() {
        return angular.fromJson($window.localStorage['tutorials']) || this.create();
      },
      update: function(key, val) {
        var tuts = this.all();
        tuts[key] = val;
        this.save(tuts);
      },
      save: function(tuts) {
        $window.localStorage['tutorials'] = angular.toJson(tuts);
      }
    }
  }
});