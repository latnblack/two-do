var twodo = angular.module('twodo')

.factory('Projects', function() {
  return {
    all: function() {
      var projectString = window.localStorage['projects'];
      if (projectString) {
        return angular.fromJson(projectString);
      }
      return [];
    },
    save: function(projects) {
      window.localStorage['projects'] = angular.toJson(projects);
    },
    saveProject: function(project) {
      var projects = this.all() || [];
      projects[project.index] = project;
      this.save(projects);
    },
    newProject: function(project) {
      var projectIndex = this.all().length;
      return {
        title: project.title,
        description: project.description,
        tasks: {
          incomplete: [],
          completed: [],
          deleted: []
        },
        index: projectIndex,
        deadline: false
      }
    },
    getLastActive: function() {
      return angular.fromJson(window.localStorage['lastActiveProject']);
    },
    setLastActive: function(project) {
      delete window.localStorage['lastActive'];
      window.localStorage['lastActiveProject'] = angular.toJson(project);
    },
    getLastActiveIndex: function() {
      return parseInt(window.localStorage['lastActiveProjectIndex']) || 0;
    },
    setLastActiveIndex: function(index) {
      window.localStorage['lastActiveProjectIndex'] = index;
    },
    getProjectByIndex: function(index) {
      var projects = this.all();
      var found = false;
      for (var i = 0; i < projects.length; i++) {
        if (i === index) {
          found = projects[i];
          found.index = i;
          break;
        }
      }
      return found;
    },
    getProjectByTitle: function(title, indexOnly) {
      var projects = this.all();
      var found = false;
      for (var i = 0; i < projects.length; i++) {
        if (projects[i].title === title) {
          found = projects[i];
          found.index = i;
          break;
        }
      }
      if (indexOnly) return found.index;
      return found;
    },

    moveTask: function(project, task, from, to) {
      var tempTasks = project.tasks;
      if (!tempTasks) return project;
      try {
        tempTasks[from].splice(task.index, 1);
        task.index = project.tasks[to].length;
        task.completed = (to == 'completed' ? Date.now() : false);
        tempTasks[to].push(task);
        tempTasks[from].forEach(function(task, index) {
          tempTasks[from][index].index = index;
        });
      }
      catch(error) {
        alert('Unable to move task: '+error.message);
        return project;
      }
      project.tasks = tempTasks;
      return this.setVisibleTasks(project);
    },

    setVisibleTasks: function(project, resetIndexes) {
      var count = 0, limit = 2, incomplete = 0;
      //console.log('in setVisibleTasks 1:', project.tasks.incomplete);
      if (project.tasks && project.tasks.incomplete) {
        project.tasks.incomplete.forEach(function(task, index) {
          //console.log('task,index:',task,index);
          if (!task.completed) incomplete++;
          project.tasks['incomplete'][index].visible = false;
          if (resetIndexes) {
            project.tasks['incomplete'][index].index = index;
          }
        });

        for (var i = 0; i < limit; i++) {
          if (project.tasks['incomplete'][i] && project.tasks['incomplete'][i] != undefined) {
            //console.log('task,i:', project.tasks['incomplete'][i]);
            //console.log('completed,incomplete', project.tasks['completed'], project.tasks['incomplete']);
            project.tasks['incomplete'][i].visible = true;
          }
        }
      }
      
      //console.log('in setVisibleTasks 2:', project.tasks.incomplete);
      return project;
    },

    removeTask: function(project, task) {
      var projects = this.all(), status = task.completed ? 'completed' : 'incomplete';
      project.tasks[status].splice([project.index], 1);
      projects[project.index] = this.setVisibleTasks(project, true);
      this.save(projects);
      this.setLastActive(projects[project.index]);
      return projects[project.index];
    },

    showAllIncomplete: function(project) {
      project.tasks.incomplete.forEach(function(task,index) {
        project.tasks.incomplete[index].visible = true;
        // console.log('hideAll'+index,project.tasks.incomplete[index]);
      });
      return project;
    },

    remove: function($scope, project) {
      // console.log('remove', project);
      if (project) {
        var projects = this.all();
        // console.log('before',projects);
        projects.splice(project.index, 1);
        projects.forEach(function(project, index) {
          projects[index].index = index;
        });
        // console.log('after',projects);
        this.save(projects);
        if (this.getLastActive() && project.index == this.getLastActive().index) {
          this.setLastActive(null);
          this.setLastActiveIndex(null);
          $scope.activeProject = null;
          $scope.selectProject(null);
        }
      }
      else {
        this.save([]);
      }
    }
  }
});